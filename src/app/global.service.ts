import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import Swal from 'sweetalert2';
const swal = Swal;
@Injectable()
export class GlobalService {

  api = "https://api.usl.edu.ph/api/";//live server
  // api = "https://apidev.usl.edu.ph/api/";//testserver
  // api="http://localhost:50315/api/";
  // api2 = "http://usl.edu.ph/pages/getphpfile/acctgapis.php/";
   api2 = "https://phpservices.usl.edu.ph/getphpfile/acctgapis.php/";
   phpService = "https://phpservices.usl.edu.ph/";
  //sy='2024251'
  sy='';
  token

  header = new Headers();
  option:any;
  email=''

  yearnow=''
  currdatearray=[]
  currentdate=0
  constructor() {

  }

  requestToken(){
    this.header.append("Content-Type", "application/json");
    this.option = new RequestOptions({ headers: this.header });
    return this.option
  }

 syDisplay(x,i=null){
    var y = x.substring(0,4)
    var z = parseInt(y) + 1
    var a = y.toString() + " - " + z.toString();
    var b = x.substring(6,7)
    var c = ''
    if(i==null){
    if (b=='1')
      c="1st Semester"
    else if (b=='2')
      c="2nd Semester"
    else if(b=='3')
      c="Summer"
    else
      c=""
    }
    return c + " SY "+a
  }


  swalAlert(title,text,type)
  {
    swal.fire({
          type: type,
          title: title,
          html: text,
           allowEnterKey:false,
         },
          )
  }

   swalInfo(title,text,type)
  {
    Swal.fire(
      title,
      text,
      type
    )
  }

  swalSuccess()
  {
    swal.fire({
          type: 'success',
          title: 'Applicant information successfully submitted.',
          html: 'Please Standby for the announcement of your schedule. Your application has been added to our database. Thank you!',
           allowEnterKey:false,
         },
          )
  }

  swalSuccess2(bat){
    swal.fire({
      type: 'success',
      title: bat,
      showConfirmButton: false,
      timer: 1500
    })
  }

  swalLoading(val){
     swal.fire({
       title: val,allowOutsideClick: false,
      });
    swal.showLoading();
  }

  swalClose(){
    swal.close();
  }

  swalAlertError(error=null)
  {
   if(error.status!=undefined)
    swal.fire('Connection Error!', 'USL Database Server may be down. Please try again later.<br><br>'+"Error reference: ("+error.status+") "+error.statusText+"<br>Target: "+error.url.replace(this.api,''), 'error');
  }
}
